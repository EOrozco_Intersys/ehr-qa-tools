"use strict";

/**
 * Current workaround until https://github.com/angular/protractor/issues/1102
 * @type {Function}
 */
var ElementFinder = $("").constructor;

// From a single select field
ElementFinder.prototype.getSingleSelectOtherButton = function() {
  var self = this;
  return self.element(by.css("button.other"));
}

// From a yes-no field
ElementFinder.prototype.getYesNoYesButton = function() {
  var self = this;
  return self.element(by.css("[data-val='true']"));
}

// From a yes-no field
ElementFinder.prototype.getYesNoNoButton = function() {
  var self = this;
  return self.element(by.css("[data-val='false']"));
}
