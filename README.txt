GETTING STARTED

    open cmd

    GLOBALLY INSTALL PROTRACTOR:
      run "npm install -g protractor"

    UPDATE WEBDRIVER MANAGER:
      run "webdriver-manager update"

    START WEBDRIVER MANAGER:
      run "webdriver-manager start"

    open a new cmd in this folder

    INSTALL MODULES:
      run "npm install"


RUN TESTS

    TO RUN ALL TESTS:
      run "npm test"

    TO RUN sandbox.spec.js:
      run "npm run-script sandbox"

    TO RUN WITH CUSTOM CREDENTIALS:
      run "npm run-script sandbox -- --params.credentials=localKevinAdminCreds"
        where 'localKevinAdminCreds' can be an credential object in eso-config.js

    TO ADD NEW TEST SCRIPTS:
      modify package.json


VIEW RESULTS
    Test results are stored in the 'reports' folder.  Open the generated report.html file to view info (including images from errors!) from a test run.


DEV GUIDLINES
    Use sandbox.spec.js to perform dev work without having to run all tests.
