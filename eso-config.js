"use strict";

var EsoConfig = {
    defaultPage: "/#/search?template=4",
    moduleName: "EHR",

    ehrDevAdminCreds: {
      baseUrl: "https://ehr-dev.dev.eso.local/ehr",
      login: "admin",
      password: ".ESOD3vDb6.",
      domain: "eso",
    },

    localKevinAdminCreds: {
      baseUrl: "https://localhost/ehr",
      login: "admin",
      password: "jenna.00",
      domain: "eso",
    },

    specs: [
        "src/ehr/validation.spec.js"
    ],
};

module.exports = EsoConfig;
