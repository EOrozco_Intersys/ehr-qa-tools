let singleSelectShelfPageObject = {
  get element() { return element(by.css("eso-single-select-shelf")); },
  clickItem(itemName) { this.element.element(by.cssContainingText("div", itemName)).click(); }, // This could return several items, but it should be ok assuming they are sorted
};

module.exports = singleSelectShelfPageObject;
