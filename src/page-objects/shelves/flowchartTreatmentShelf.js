let flowchartTreatmentShelfPageObject = {
  get element() { return element(by.css("flow-chart-treatment")); },

  close() {
    var okButton = this.element.element(by.buttonText("OK"));
    okButton.click();

    let pcr = require(protractor.pageObjectDir + "pcr");
    pcr.waitOnShelf();
  },

  get sizeSingleSelect() { return element(by.css("[field-config='flowchartTreatments.treatments.size']")); },
  get providerSingleSelect() { return element(by.css("[field-config='flowchartTreatments.treatments.provider']")); },
  get complicationSingleSelect() { return element(by.css("[field-config='flowchartTreatments.treatments.genInfoComplications']")); },
};

module.exports = flowchartTreatmentShelfPageObject;
