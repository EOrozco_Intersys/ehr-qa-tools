let incidentTabPageObject = {
  get runTypeSingleSelect() { return element(by.css("[field-config='incident.response.runTypeId']")); },
  get dispositionSingleSelect() { return element(by.css("[field-config='incident.disposition.dispositionItemID']")); },
  get emdComplaintSingleSelect() { return element(by.css("[field-config='incident.response.emdComplaintId']")); },
};

module.exports = incidentTabPageObject;
