let patientTabPageObject = {
  get firstNameTextInput() { return element(by.css("[field-config='patient.demographics.firstName'] input")); },
  get presenceOfEmergencyFormYesNo() { return element(by.css("[field-config='patient.contact.presenceOfEmergencyForm']")); },
};

module.exports = patientTabPageObject;
