let flowchartTabPageObject = {
  get airwayButton() { return element(by.buttonText("Airway")); },
  get criticalCareButton() { return element(by.buttonText("Critical Care")); },
  get defibCardioButton() { return element(by.buttonText("Defib/Cardio")); },
  get ivTherapyButton() { return element(by.buttonText("IV Therapy")); },
  get medicationButton() { return element(by.buttonText("Medication")); },
  get otherButton() { return element(by.buttonText("other")); },
  get importButton() { return element(by.buttonText("Import")); },

  get flowchartTreatmentRows() { return $$("flow-chart-grid grid-row[data-key]"); }
};

module.exports = flowchartTabPageObject;
