let validationToastPageObject = {
    get element() { return $("validation-toast shelf-panel"); },
    get okButton() { return this.element.element(by.buttonText("OK")); },
    waitReady() { return this.element.waitReady(); },
    validationError(label, description)
    {
      return this.element.all(by.css("grid-row"))         // get all triggered rules
                         .filter(function(elem, index){   // filter by rules that have an element with text matching the label
                           return elem.all(by.cssContainingText("*", label)).then(function(arr){ return arr.length > 0; });
                         })
                         .filter(function(elem, index){   // filter by rules that have an element with text matching the description
                           return elem.all(by.cssContainingText("*", description)).then(function(arr){ return arr.length > 0; });
                         })
                         .first();
    },
};

module.exports = validationToastPageObject;
