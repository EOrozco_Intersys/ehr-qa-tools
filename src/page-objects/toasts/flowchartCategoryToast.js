let flowchartToastPageObject = {
  get element() { return element(by.css("flow-chart-treatments")); },

  close() {
    var okButton = this.element.element(by.buttonText("OK"));
    okButton.click();

    let pcr = require(protractor.pageObjectDir + "pcr");
    pcr.waitOnShelf();
  },

  treatmentButton(treatmentName) { return this.element.element(by.cssContainingText("button", treatmentName)); },
};

module.exports = flowchartToastPageObject;
