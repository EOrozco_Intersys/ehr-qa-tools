let pcrGridPageObject = {
  get newRecordButton() { return element(by.buttonText("New Record")) },
};

module.exports = pcrGridPageObject;
