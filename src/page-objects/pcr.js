let pcrPageObject = {
  get incidentTabButton() { return $("[ui-sref='pcr.incident']"); },
  get patientTabButton() { return $("[ui-sref='pcr.patient']"); },
  get vitalsTabButton() { return $("[ui-sref='pcr.vitals']"); },
  get flowchartTabButton() { return $("[ui-sref='pcr.flowchart']"); },
  get assessmentsTabButton() { return $("[ui-sref='pcr.assessments']"); },
  get narrativeTabButton() { return $("[ui-sref='pcr.narrative']"); },
  get formsTabButton() { return $("[ui-sref='pcr.forms']"); },
  get billingTabButton() { return $("[ui-sref='pcr.billing']"); },
  get signaturesTabButton() { return $("[ui-sref='pcr.signatures']"); },

  get validateButton() { return $("button.validate"); },

  waitOnShelf() {
    var shelf = element(by.css("eso-shelf.ng-animate"));
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.stalenessOf(shelf), 5000); // Wait until there is no animating shelf
  },
};

module.exports = pcrPageObject;
