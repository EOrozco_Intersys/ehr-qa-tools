/**
 * Test validation rules
 */
let pageName = "Validation";

describe(pageName, function () {
  let pcrGrid = require(protractor.pageObjectDir + "pcrGrid");
  let pcr = require(protractor.pageObjectDir + "pcr");
  let patientTab = require(protractor.pageObjectDir + "pcr-tabs/patientTab");
  let incidentTab = require(protractor.pageObjectDir + "pcr-tabs/incidentTab");
  let validationToast = require(protractor.pageObjectDir + "toasts/validationToast");
  let singleSelectShelf = require(protractor.pageObjectDir + "shelves/singleSelectShelf")
  let EsoConfig = require(protractor.baseDir + "eso-config");

  /** ESO Initializer, which handles the loading and unloading of pages. */
  let Initializer = require(protractor.baseDir + "eso-initializer");

  /**
   * The Expected Conditions library from Protractor, which is useful
   * for checking the existence and visibility of elements on the DOM.
   */
  let toSee = protractor.ExpectedConditions;

  describe("Required Fields", () => {

    // Create new pcr before each 'it'
    beforeEach(done => {
        Initializer.loadIncidentGridAndCreatePcr().then(done);
    });

    it("Disposition", done => {
        pcr.validateButton.click();
        var rule = validationToast.validationError("Disposition", "Required");
        expect(rule.isPresent()).toBe(true);
        rule.click();

        singleSelectShelf.clickItem("Transported Lights/Siren");
        pcr.validateButton.click();
        expect(rule.isPresent()).toBe(false);
        validationToast.okButton.click();

        done();
    });

    it("On Scene", done => {
        pcr.validateButton.click();
        var rule = validationToast.validationError("First Name", "Required");
        expect(rule.isPresent()).toBe(false);
        validationToast.okButton.click();

        incidentTab.dispositionSingleSelect.getSingleSelectOtherButton().click();
        singleSelectShelf.clickItem("Transported Lights/Siren");
        pcr.validateButton.click();
        expect(rule.isPresent()).toBe(true);
        rule.click();

        patientTab.firstNameTextInput.sendKeys("Mr. Automation");
        pcr.validateButton.click();
        expect(rule.isPresent()).toBe(false);
        validationToast.okButton.click();

        done();
    });
  });
});
