/**
 * For experimenting and developing new tests
 */
let pageName = "Sandbox";

describe(pageName, function () {
  let pcrGrid = require(protractor.pageObjectDir + "pcrGrid");
  let pcr = require(protractor.pageObjectDir + "pcr");
  let patientTab = require(protractor.pageObjectDir + "pcr-tabs/patientTab");
  let incidentTab = require(protractor.pageObjectDir + "pcr-tabs/incidentTab");
  let flowchartTab = require(protractor.pageObjectDir + "pcr-tabs/flowchartTab");
  let flowchartCategoryToast = require(protractor.pageObjectDir + "toasts/flowchartCategoryToast");
  let flowchartTreatmentShelf = require(protractor.pageObjectDir + "shelves/flowchartTreatmentShelf");
  let validationToast = require(protractor.pageObjectDir + "toasts/validationToast");
  let singleSelectShelf = require(protractor.pageObjectDir + "shelves/singleSelectShelf")
  let EsoConfig = require(protractor.baseDir + "eso-config");

  /** ESO Initializer, which handles the loading and unloading of pages. */
  let Initializer = require(protractor.baseDir + "eso-initializer");

  /**
   * The Expected Conditions library from Protractor, which is useful
   * for checking the existence and visibility of elements on the DOM.
   */
  let toSee = protractor.ExpectedConditions;

  // Create new pcr
  beforeEach(done => {
      Initializer.loadIncidentGridAndCreatePcr().then(done);
  });

  it("Stuff", done => {
      // Use this to test your code without running all tests

      done();
  });

});
