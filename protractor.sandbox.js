/**
 * The Protractor configuration file for developing tests
 */

// Pull the ESO-Config, where we define test specs.
let EsoConfig = require("./eso-config");

exports.config = {
    framework: "jasmine",
    seleniumAddress: "http://localhost:4444/wd/hub",

    specs:  [
        "src/ehr/sandbox.spec.js"
      ],

    onPrepare: function () {
        // The Initializer, which handles ESO-centric methods of loading pages.
        const Initializer = require("./eso-initializer");

        // Run shared initialization steps, such as adding new prototypal (extension) methods,
        // setting some protractor properties, and adding reporters to Jasmine.
        Initializer.onPrepare();

        // Log in to the ESO Suite. Return makes it async, preventing us from
        // starting tests until the promise resolves.
        return Initializer.loginToEso();
    },

    // Browser capabilities.
    capabilities: {
        name: "Win10/Chrome",
        platform: "Windows 10",
        browserName: "chrome",
        'chromeOptions': { args: ["--test-type", "--ignore-certificate-errors"] }
    },

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        print: function () { /* Prevent the default spec dots from appearing in the output */ }
    }

}
