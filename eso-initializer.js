let EsoConfig = require("./eso-config");

// The Expected Conditions library from Protractor, which is useful for checking
// the existence and visibility of elements on the DOM.
let toSee = protractor.ExpectedConditions;

let Initializer = {
    activeCredentials: EsoConfig.ehrDevAdminCreds,

    onPrepare: function() {
        var credentialsParam = browser.params.credentials;
        if(credentialsParam){
          if(EsoConfig[credentialsParam]){
            console.log("Using credentials: " + credentialsParam);
            this.activeCredentials = EsoConfig[credentialsParam];
          } else {
            console.log("Unrecognized credentials name: " + credentialsParam);
          }
        } else {
          console.log("Using default credentials");
        }

        // Load our helper libraries, these add some custom extension methods.
        require("./helpers/wait-absent.js");
        require("./helpers/wait-ready.js");
        require("./helpers/date-helpers.js");
        require("./helpers/field-helpers.js");

        // Define base path in order to load Page Objects.
        protractor.baseDir = __dirname + "/";
        protractor.pageObjectDir = protractor.baseDir + "src/page-objects/";
        protractor.sharedTestsDir = protractor.baseDir + "src/shared-tests/";

        // Add a reporter that outputs to log.
        const SpecReporter = require("jasmine-spec-reporter").SpecReporter;
        jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));

        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
        const HtmlReporter = require("protractor-beautiful-reporter");
        jasmine.getEnv().addReporter(new HtmlReporter({
            baseDirectory: "./reports/" + `${new Date().format("{FullYear}{Month:2}{Date:2}-{Hours:2}{Minutes:2}{Seconds:2}")}`,
            screenshotsSubfolder: "images",
            jsonsSubfolder: "json",
            docName: "report.html",
            docTitle: "EHR Regression Tests",
            takeScreenShotsOnlyForFailedSpecs: true
        }).getJasmine2Reporter());

        browser.manage().timeouts().implicitlyWait(200);
        browser.driver.manage().window().maximize();
    },

    loginToEso: function () {
        console.log("Logging in to ESO Suite");

        // Disable synchronization for non-Angular login screen.
        browser.ignoreSynchronization = true;
        browser.driver.get(this.activeCredentials.baseUrl);

        var creds = this.activeCredentials;

        // If we aren't on the login page, we are running locally and don't have to login
        element(by.css("#UserName")).isPresent().then(function(inDom){
          if(!inDom)
            return;

          // Login to ESO.
          element(by.css("#UserName")).sendKeys(creds.login);
          element(by.css("#Password")).sendKeys(creds.password);
          element(by.css("#AgencyLoginId")).sendKeys(creds.domain);
          element(by.buttonText("Let's Go!")).click();
        });

        // Wait for the page to be loaded.
        browser.wait(toSee.presenceOf($("incident-grid")), 10000);

        // Navigate to the default URL.
        //browser.driver.get(EsoConfig.baseUrl + EsoConfig.defaultPage);

        // wait for deferred bootstrap body to not be present
        return element(by.css("body.deferred-bootstrap-loading")).waitAbsent()
        .then(() =>{
          browser.waitForAngular();
          browser.ignoreSynchronization = false;
        });
    },

    loadPage: function (pageUrl, pageDirective) {
        // Deactivate synch with Angular for manual boostrap.
        browser.ignoreSynchronization = true;

        // Navigate to the current page.
        browser.driver.get(this.activeCredentials.baseUrl + pageUrl);

        // wait for deferred bootstrap body to not be present
        return browser.wait(toSee.presenceOf($(pageDirective)), 3000)
        .then(() => {
            browser.waitForAngular();
            browser.ignoreSynchronization = false;
        });
    },

    unloadPage: function () {
        // Remove local and session storage to begin with clean slate.
        browser.executeScript("window.sessionStorage.clear();");
        browser.executeScript("window.localStorage.clear();");
    },

    loadIncidentGrid: function (){
      return this.loadPage(EsoConfig.defaultPage, "incident-grid");
    },

    loadIncidentGridAndCreatePcr: function (){
      let pcrGrid = require(protractor.pageObjectDir + "pcrGrid");
      let toSee = protractor.ExpectedConditions;
      this.loadIncidentGrid().then(pcrGrid.newRecordButton.click);
      return browser.wait(toSee.presenceOf($("incident-tab")), 3000);
    }
};

module.exports = Initializer;
